<?php

// Bitwise 'not'

$a = 4;
$b = ~$a;
$c = ~$a < 0;

echo "Output\n----------\n";
echo '$a ->  ', $a, " -> '", sprintf("%'064s", decbin($a)), "'\n";
echo '$b -> ',  $b, " -> '", sprintf("%'064s", decbin($b)), "'\n";
echo '$c -> ', var_export($c, true), "\n\n";

/*
Output:
----------
$a ->  4 -> '0000000000000000000000000000000000000000000000000000000000000100'
$b -> -5 -> '1111111111111111111111111111111111111111111111111111111111111011'
$c -> true
*/


// Bitwise 'or' and 'and' 

$c = $a | ~$a;
$d = $a & ~$a;

echo "Output\n----------\n";
echo '$a ->  ', $a, " -> '", sprintf("%'064s", decbin($a)), "'\n";
echo '$b -> ',  $b, " -> '", sprintf("%'064s", decbin($b)), "'\n";
echo '$c -> ',  $c, " -> '", sprintf("%'064s", decbin($c)), "'\n";
echo '$d ->  ', $d, " -> '", sprintf("%'064s", decbin($d)), "'\n\n";

/*
Output:
----------
$a ->  4 -> '0000000000000000000000000000000000000000000000000000000000000100'
$b -> -5 -> '1111111111111111111111111111111111111111111111111111111111111011'
$d -> -1 -> '1111111111111111111111111111111111111111111111111111111111111111'
$e ->  0 -> '0000000000000000000000000000000000000000000000000000000000000000'
*/


// String concatenation: '.' -> '~'

$a = "abc";
$b = $a ~ ~0;
$c = $b; 
$c ~= "bcd" ~ ~4;

echo "Output\n----------\n";
echo '$a -> \'', $a, "'\n";
echo '$b -> \'', $b, "'\n";
echo '$c -> \'', $c, "'\n\n";

/*
Output
----------
$a -> 'abc'
$b -> 'abc-1'
$c -> 'abc-1bcd-5'
*/


// Access to array members: '=>' -> ':='

$a = [ "a" := 1, "b" := 2 ];

echo "Output\n----------\n";
echo '$a -> ', var_export($a, true), "\n\n";

/*
Output:
----------
$a -> array (
  'a' => 1,
  'b' => 2,
)
*/


// List construct: '=>' -> ':='

[1 := $oneBit, 2 := $twoBit, 3 := $threeBit] = [1 := 2, 2 := 4, 3 := 8];

echo "Output\n----------\n";
echo '$oneBit -> '; var_export($oneBit); echo "\n";
echo '$twoBit -> '; var_export($twoBit); echo "\n";
echo '$threeBit -> '; var_export($threeBit); echo "\n\n";

/*
Output
----------
$oneBit -> 2
$twoBit -> 4
$threeBit -> 8
*/


// Access to object members: '->' -> '.'

class Test {

  public function concat($left, $right)
  {
    $result = $left; 
    $result ~= ", " ~ $right;
    return $result;
  }
  
  public function hello()
  {
    return "Hello!";
  }

}

$test = new Test();

echo "Output\n----------\n";
echo '$test.hello() -> '; var_export($test.hello()); echo "\n";
echo '$test.concat("Hello", "world") -> '; var_export($test.concat("Hello", "world")); echo "\n\n";

/*
Output
----------
$test.hello() -> 'Hello!'
$test.concat("Hello", "world") -> 'Hello, world'
*/

